fetch('https://hsgr.gitlab.io/website/members.json', {
    method: 'get',
    cache: "no-cache",
}).then((response) => response.json())
.then((data) => {
  var members = []
  data.forEach(member => {
    members.push(member.alias)
  })
  render(members);
})
.catch(function(err) {
  console.log("Error geting members")
});

// window.localStorage.clear()
present = []; absent = []; optout = []; foptout = [];  poptout = [];
var md = "";

function render(members){
  var member_count = members.length
  members.forEach(addOption, member_count)
  document.querySelector('#membercount').innerHTML = member_count
  // Populate optouts
  poptout = JSON.parse(localStorage.getItem("poptout"));
  foptout = JSON.parse(localStorage.getItem("foptout"));
  try {
    poptout.forEach((item, i) => {
      document.getElementById(item+'_poptout').checked = true
    });
    foptout.forEach((item, i) => {
      document.getElementById(item+'_foptout').checked = true
    });
  } catch (error) {
    console.log("Local storage empty");
  }
  enumerate(member_count);
  // Clipboard handle
  let handleCopyClick = document.querySelector('#copy-md');
  handleCopyClick.addEventListener('click', () => {
    navigator.clipboard.writeText(`${md}`);
    // Snackbar
    var sb = document.getElementById("snackbar");
    sb.className = "show";
    setTimeout(function(){ sb.className = sb.className.replace("show", ""); }, 3000);
    // Store optouts
    localStorage.setItem("poptout", JSON.stringify(poptout));
    localStorage.setItem("foptout", JSON.stringify(foptout));
  });
}

// Generate option items
function addOption(item) {
  var member_count = this.valueOf();
  console.log(member_count)

  optionItem = "<div class='optionrow'>"
  optionItem += `<div class='member_name'>${item}:</div><div class='member_options'>`
  optionItem += `<label><input checked onclick='enumerate(${member_count})' type=radio id='${item}_absent' name='${item}' value='absent'>Absent</label>`
  optionItem += `<label><input onclick='enumerate(${member_count})' type=radio id='${item}_present' name='${item}' value='present'>Present</label>`
  optionItem += `<label><input onclick='enumerate(${member_count})' type=radio id='${item}_optout' name='${item}' value='optout'>Opt Out</label>`
  optionItem += `<label><input onclick='enumerate(${member_count})' type=radio id='${item}_poptout' name='${item}' value='poptout'>Perm Opt Out</label>`
  optionItem += `<label><input onclick='enumerate(${member_count})' type=radio id='${item}_foptout' name='${item}' value='foptout'>Forced Opt Out</label></div>`
  optionItem += "</div>"
  document.querySelector('#memberlist').innerHTML+=optionItem;
}
// Enumerate inputs
function enumerate(member_count) {
  present = []; absent = []; optout = []; foptout = [];  poptout = [];
  var ele = document.getElementsByTagName('input');
  for(i = 0; i < ele.length; i++) {
    if (ele[i].checked) {
      // console.log(ele[i], i, ele[i].name, ele[i].value)
      window[ele[i].value].push(ele[i].name)
    }
  };
  // Update HTML
  document.querySelector('#presentlist').innerHTML = present.length + " - " + present.join(", ")
  document.querySelector('#absentlist').innerHTML = absent.length + " - " + absent.join(", ")
  document.querySelector('#optoutlist').innerHTML = optout.length + " - " + optout.join(", ")
  document.querySelector('#poptoutlist').innerHTML = poptout.length + " - " + poptout.join(", ")
  document.querySelector('#foptoutlist').innerHTML = foptout.length + " - " + foptout.join(", ")
  document.querySelector('#decisionstatus').innerHTML = (present.length + optout.length + poptout.length + foptout.length) >= member_count/2
  document.querySelector('#unmemberstatus').innerHTML = (present.length - optout.length - poptout.length - foptout.length) >= member_count/2
  // Generate markdown
  md = "## Assembly "+ (new Date().toISOString().split('T')[0]) +"  \n";
  md += `* Members (**${present.length}**): ${present.join(", ")}\n`
  md += `* Μόνιμο opt-out (**${poptout.length}**): ${poptout.join(", ")}\n`
  md += `* Forced opt-out (**${foptout.length}**): ${foptout.join(", ")}\n`
  md += `* Opt-out (**${optout.length}**): ${optout.join(", ")}\n`
  md += `* Non-participants (**${absent.length}**): ${absent.join(", ")}\n`
}
