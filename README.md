# HSGR Assembly participation logger

A tool for easily recording participants in HSGR Assemblies  
Generates markdown for wiki pages  
Forced and Permanent opt-outs are stored in browser storage  

## Usage
You click the names  
You click "Copy" to get markdown and store optouts

## TODO
Get member list from a centralized source  
Store info in a DB for logging and forced opt-out calculation
